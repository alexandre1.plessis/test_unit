<?php

namespace App\Test;

use App\User;

class UserTest extends \PHPUnit\Framework\TestCase
{
    public function testTellName()
    {
        $user = new User(20, "John");
        $this->assertEquals("My name is John.", $user->tellName());
    }

    public function testTellAge()
    {
        $user = new User(20, "John");
        $this->assertEquals("I am 20 years old.", $user->tellAge());
    }

    public function testAddFavoriteMovie()
    {
        $user = new User(20, "John");
        $user->addFavoriteMovie("The Matrix");
        $this->assertEquals(["The Matrix"], $user->favorite_movies);
    }

    public function testRemoveFavoriteMovie()
    {
        $user = new User(20, "John");
        $user->addFavoriteMovie("The Matrix");
        $user->removeFavoriteMovie("The Matrix");
        $this->assertEquals([], $user->favorite_movies);
    }

    public function testRemoveFavoriteMovieThrowsException()
    {
        $user = new User(20, "John");
        $this->expectException(\InvalidArgumentException::class);
        $user->removeFavoriteMovie("The Matrix");
    }
}